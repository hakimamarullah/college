from django.urls import path

from . import views

app_name = 'myscheduleApp'

urlpatterns = [
    path('', views.index),
]
