from django import forms
from .models import MataKuliah

class Subject(forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = '__all__'

		widgets ={
			'nama_matkul':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Nama Matkul'}),
			'dosen':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Dosen'}),
			'sks':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Jumlah SKS'}),
			'deskripsi':forms.Textarea(attrs={'class':'form-control', 'placeholder' : 'Write Subject Description Here'}),
			'term':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'e.g Gasal 2019/2020'}),
			'ruang_kelas':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Ruang Kelas'}),
		}