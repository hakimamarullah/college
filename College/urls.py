"""College URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from myscheduleApp import views
from Activity import views as views_activity
from accordion import views as views_accordion
from BooksLib import views as views_bookslib
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('myscheduleApp.urls')),
    path('addnew', views.addnew),
    path('edit/<int:id>', views.edit),
    path('update/<int:id>', views.update),
    path('delete/<int:id>', views.destroy),
    path('activity', views_activity.activity),
    path('add/<int:id>', views_activity.add),
    path('deleteact/<int:id>', views_activity.delete),
    path('addact', views_activity.addact),
    path('accordion', views_accordion.accordion),
    path('bookslib', views_bookslib.bookslib),
    path('data', views_bookslib.parseData),
]