from django.apps import AppConfig


class BookslibConfig(AppConfig):
    name = 'BooksLib'
