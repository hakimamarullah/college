from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
# Create your views here.
def bookslib(request):
	context = {}
	return render(request, 'booksLib.html', context)
def parseData(request):
	keyword = request.GET['q']
	url = 'https://www.googleapis.com/books/v1/volumes?q=%s' %keyword
	request_data = requests.get(url)
	data = json.loads(request_data.content)
	return JsonResponse(data, safe=False)