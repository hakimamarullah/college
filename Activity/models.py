from django.db import models

# Create your models here.
class Activity(models.Model):
	activity = models.CharField(max_length=50)

	class Meta:
		db_table = 'Activities'
		ordering = ['activity']
	
	def __str__(self):
		return self.activity

class Person(models.Model):
	 name = models.CharField(max_length=25)
	 activities = models.ManyToManyField(Activity)

	 class Meta:
	 	db_table = 'Persons'

	 def __str__(self):
	 	return self.name