from django import forms
from .models import Activity, Person

class ActivityForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = ('activity',)

		widgets ={
			'activity':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Activity'}),
			
		}
class PersonForm(forms.ModelForm):
	class Meta:
		model = Person
		fields = ('name',)

		widgets ={
			'name':forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Name'}),
			
		}