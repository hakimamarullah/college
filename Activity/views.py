from django.shortcuts import render, redirect
from .models import Activity, Person
from .forms import PersonForm, ActivityForm

# Create your views here.
def activity(request):
	activity = Activity.objects.all()
	form = PersonForm()
	form_act = ActivityForm()
	context ={
		'activities':activity,
		'form':form,
		'form_act':form_act,
	}
	return render(request, 'activity.html',context)

def add(request, id):
	if request.method == "POST":
		form=PersonForm(request.POST)
		if form.is_valid():
			person = Person(name=form.cleaned_data['name'])
			person.save()
			person.activities.add(Activity.objects.get(id=id))
			return redirect('/activity')
	else:
		form=PersonForm()
	return render(request, 'activity.html')

def delete(request, id):
	Person.objects.filter(activities=Activity.objects.get(id=id)).delete()
	Activity.objects.get(id=id).delete()
	return redirect('/activity')

def addact(request):
	if request.method == "POST":
		form = ActivityForm(request.POST)
		if form.is_valid():
			activity = Activity(activity=form.cleaned_data['activity'])
			activity.save()
			return redirect('/activity')
	else:
		form = ActivityForm()
	return render(request, 'activity.html')