# Generated by Django 3.1.2 on 2020-10-22 12:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Activity', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='activity',
            table='Activities',
        ),
        migrations.AlterModelTable(
            name='person',
            table='Persons',
        ),
    ]
